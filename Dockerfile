FROM rust:1-alpine3.14 AS builder
WORKDIR /root/src
RUN apk add --no-cache build-base
ADD Cargo.lock Cargo.toml ./
ADD src ./src
RUN cargo build --release

FROM scratch
ENV DISCORD_TOKEN RUST_LOG
COPY --from=builder /root/src/target/release/gauss /gauss
USER 1000
ENTRYPOINT ["/gauss"]
