{
  description = "A discord bot for rendering mathematical equations.";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    let
      gaussVersion = "2.2.0";

      buildGauss = { pname, version, pkgs, src, extensions }:
        pkgs.rustPlatform.buildRustPackage {
          buildFeatures = if extensions then [ "extensions" ] else [ ];
          cargoLock.lockFile = ./Cargo.lock;
          inherit pname version src;
        };
    in flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages.default = buildGauss {
          pname = "gauss";
          version = gaussVersion;

          src = ./.;
          extensions = false;
          inherit pkgs;
        };
      }) // {
        lib.withExtensions = { pname, version, src, system }:
          let
            pkgs = nixpkgs.legacyPackages.${system};
            completeVersion = "${version}+gauss${gaussVersion}";
          in buildGauss {
            version = completeVersion;

            src = pkgs.stdenv.mkDerivation {
              name = "${pname}-${completeVersion}-source";
              phases = [ "installPhase" ];

              installPhase = ''
                mkdir -p "$out"/src/extensions
                cp -rT '${./.}' "$out"
                cp -rT '${src}' "$out"/src/extensions
              '';
            };
            extensions = true;

            inherit pname pkgs;
          };
      };
}
