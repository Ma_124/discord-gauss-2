use log::{debug, error, info};
use poise::samples::register_globally;
use poise::serenity_prelude::{ActivityData, ClientBuilder, FullEvent, GatewayIntents};
use poise::{
    say_reply, serenity_prelude as serenity, EditTracker, FrameworkContext, FrameworkError,
};
use poise::{BoxFuture, Framework, FrameworkOptions, PrefixFrameworkOptions};
use std::default::Default;
use std::env;
use std::process::ExitCode;
use std::time::Duration;

#[cfg(feature = "extensions")]
mod extensions;

#[cfg(not(feature = "extensions"))]
mod extensions {
    use poise::Command;

    use crate::{Data, Error};

    pub const ENABLED: bool = false;

    pub fn commands(commands: Vec<Command<Data, Error>>) -> Vec<Command<Data, Error>> {
        commands
    }
}

type Data = ();
type Error = Box<dyn std::error::Error + Send + Sync>;
type Context<'a> = poise::Context<'a, Data, Error>;

#[tokio::main]
async fn main() -> ExitCode {
    env_logger::init();

    let token = match env::var("DISCORD_TOKEN") {
        Ok(token) => token,
        Err(err) => {
            error!("no token provided ({})", err);
            return ExitCode::FAILURE;
        }
    };

    info!("connecting");
    let fw = Framework::builder()
        .options(FrameworkOptions {
            on_error,
            event_handler: on_event,
            prefix_options: PrefixFrameworkOptions {
                prefix: Some("%".into()),
                edit_tracker: Some(EditTracker::for_timespan(Duration::from_secs(60 * 60)).into()),
                ..Default::default()
            },
            commands: extensions::commands(vec![tex(), help()]),
            ..Default::default()
        })
        .setup(|ctx, _ready, fw| {
            Box::pin(async move {
                register_globally(ctx, &fw.options().commands).await?;
                Ok(())
            })
        })
        .build();

    let mut client = match ClientBuilder::new(token, GatewayIntents::GUILDS | GatewayIntents::GUILD_MESSAGES | /* privileged, required for prefix commands */ GatewayIntents::MESSAGE_CONTENT)
        .framework(fw)
        .await
    {
        Ok(client) => client,
        Err(err) => {
            error!("building client: {err}");
            return ExitCode::FAILURE;
        }
    };

    match client.start().await {
        Ok(()) => (),
        Err(err) => {
            error!("crash: {err}");
            return ExitCode::FAILURE;
        }
    }

    return ExitCode::SUCCESS;
}

fn on_error(err: FrameworkError<'_, Data, Error>) -> BoxFuture<'_, ()> {
    Box::pin(async move {
        error!("error: {err}");
        match err {
            FrameworkError::EventHandler { event, .. } => {
                error!("on_event error: {err} (Event: {event:#?})");
            }
            FrameworkError::Command { ctx, .. } => {
                _ = say_reply(ctx, "Some error occurred.").await;
            }
            FrameworkError::CommandPanic { ctx, .. } => {
                _ = say_reply(ctx, "Some error occurred.").await;
            }
            FrameworkError::ArgumentParse { ctx, .. } => {
                _ = say_reply(ctx, "Command could not be parsed.").await;
            }
            _ => {}
        };
    })
}

fn on_event<'a>(
    ctx: &'a serenity::Context,
    ev: &'a FullEvent,
    _fw: FrameworkContext<'a, Data, Error>,
    _data: &'a Data,
) -> BoxFuture<'a, Result<(), Error>> {
    Box::pin(async move {
        match ev {
            FullEvent::Ready { data_about_bot } => {
                info!(
                    "ready, app_id={}, user_id={}, api_version={}, version={}, renderer={}, extensions={}",
                    data_about_bot.application.id, data_about_bot.user.id, data_about_bot.version, env!("CARGO_PKG_VERSION"), renderer::NAME, extensions::ENABLED
                );
                ctx.set_activity(Some(ActivityData::custom("A bot to render TeX (%help)")));
            }
            FullEvent::GuildCreate { guild, .. } => {
                info!("part of guild {:?} ({})", guild.name, guild.id)
            }
            _ => {}
        }
        Ok(())
    })
}

/// Format LaTeX
#[poise::command(prefix_command, slash_command, broadcast_typing, track_edits)]
pub async fn tex(
    ctx: Context<'_>,
    #[rest]
    #[description = "LaTeX to compile"]
    tex: Option<String>,
) -> Result<(), Error> {
    let tex = tex.unwrap_or_default();
    debug!(
        "%tex called for string length {} in {}",
        tex.len(),
        ctx.guild_id().unwrap_or_default(),
    );

    let tex = tex.trim_matches(|c: char| c.is_whitespace() || c == '`');
    if tex.is_empty() {
        ctx.say(
            r#"usage: ```%tex `x = {-b \pm \sqrt{b^2-4ac} \over 2a}` ```
Call `%help` to get a short introduction into TeX."#,
        )
        .await?;
        return Ok(());
    }

    renderer::render(ctx, tex).await?;

    Ok(())
}

/// Show help menu
#[poise::command(prefix_command, slash_command, track_edits)]
pub async fn help(ctx: Context<'_>) -> Result<(), Error> {
    debug!("%help called in {}", ctx.guild_id().unwrap_or_default());
    ctx.say(format!(r#"`%tex <tex>`
Render TeX Math.

*Example:*
```%tex `x = \frac{{-b \pm \sqrt{{b^2-4ac}}}}{{2a}}` ```
*Short introduction to LaTeX's math syntax:*
Many operators work as expected (e.g. `a+b`, `a < b > c`).

`a \cdot b`: a · b (**c**enter **dot**)
`a \times b`: a × b
`a^2`: a²
`a_i`: aᵢ
`\frac{{1}}{{2}}`: ½ (**frac**tion)
`\sqrt{{-1}}`: √-1 (**sq**uare **r**oo**t**)
`\alpha`: 𝛼
`\forall a \in N. \exists b`: ∀a∈N. ∃b
`a \leq b \geq c`: a ≤ b ≥ c (**l**ess / **g**reater than or **eq**ual)
`a \land b \lor c`: a∧b∨c (**l**ogical and/or)
`\Sum_{{i=0}}^n`: ∑ⁿᵢ₌₀
`\sin (x)`: sin(x)
`\text{{op}}(x)`: op(x)

`{{}}` can be used for grouping things in super- and subscripts (powers and indices). If you want literal `{{}}` use `\{{\}}`.

More information can be found at https://en.wikibooks.org/wiki/LaTeX/Mathematics
Rendering is done by *{}*
"#, renderer::NAME)).await?;
    Ok(())
}

#[cfg(not(feature = "mathjax_api"))]
mod renderer {
    use percent_encoding::{utf8_percent_encode, NON_ALPHANUMERIC};

    use crate::{Context, Error};

    pub const NAME: &str = "Google Charts API";

    pub async fn render(ctx: Context<'_>, tex: &str) -> Result<(), Error> {
        ctx.say(format!(
            "https://chart.googleapis.com/chart?cht=tx&chf=bg,s,36393F00&chco=FFFFFF&chl={}",
            utf8_percent_encode(tex, NON_ALPHANUMERIC),
        ))
        .await?;
        Ok(())
    }
}

#[cfg(feature = "mathjax_api")]
mod renderer {
    use poise::{serenity_prelude::CreateAttachment, CreateReply};
    use tiny_skia::Pixmap;
    use usvg::{FitTo, ImageRendering, OptionsRef, ShapeRendering, Size, TextRendering, Tree};

    use crate::{Context, Error};

    pub const NAME: &str = "MathJax";

    pub async fn render(ctx: Context<'_>, tex: &str) -> Result<(), Error> {
        // TODO protect against DOS by passing large TeX strings
        let svg = reqwest::Client::new()
            .get("https://math.now.sh")
            .query(&[("from", tex), ("color", "#C9CBCC")])
            .send()
            .await?
            .error_for_status()?
            .text()
            .await?;

        let png = render_svg(&svg)?;

        ctx.send(CreateReply::default().attachment(CreateAttachment::bytes(png, "tex.png")))
            .await?;

        Ok(())
    }

    fn render_svg(svg: &str) -> Result<Vec<u8>, Error> {
        const DEFAULT_WIDTH: u32 = 500;
        const DEFAULT_HEIGHT: u32 = 100;

        let svg = Tree::from_str(
            svg,
            &OptionsRef {
                resources_dir: None,
                dpi: 72.0,
                font_family: "serif",
                font_size: 16.0,
                languages: &[],
                shape_rendering: ShapeRendering::GeometricPrecision,
                text_rendering: TextRendering::GeometricPrecision,
                image_rendering: ImageRendering::OptimizeQuality,
                keep_named_groups: false,
                default_size: Size::new(DEFAULT_WIDTH as f64, DEFAULT_HEIGHT as f64).unwrap(),
            },
        )?;

        let mut pixmap =
            Pixmap::new(DEFAULT_WIDTH, DEFAULT_HEIGHT).ok_or("could not construct pixmap")?;

        resvg::render(&svg, FitTo::Original, pixmap.as_mut()).ok_or("could not render SVG")?;
        let pixmap = resvg::trim_transparency(pixmap)
            .ok_or("could not trim transparency")?
            .2;

        Ok(pixmap.encode_png()?)
    }
}
